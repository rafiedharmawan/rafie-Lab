// FB initiation function
window.fbAsyncInit = function() {
    FB.init({
        appId      : '1780168788702710',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.11'
    });
    FB.AppEvents.logPageView();   

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected'){
            render(true);
        } else {
            render(false);
        }
    })
};

// Call init facebook. default dari facebook
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
    if (loginFlag) {
        // Jika yang akan dirender adalah tampilan sudah login

        // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
        // yang menerima object user sebagai parameter.
        // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
        getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#my_facebook').html(
            '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
                '<h1>' + user.name + '</h1>' +
                '<h2>' + user.about + '</h2>' +
                '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
            '<button class="logout btn" onclick="facebookLogout()">Logout</button>' +
            '</div>' +
            
            '<div class=status-form>' +
            '<input id="postInput" type="text" class="form-control" row="5" placeholder="Apa yang Anda pikirkan sekarang?" />' +
            '<button class="postStatus btn" id="post-button" onclick="postStatus(postInput.value)">Post ke Facebook</button>' +
            '</div>'

        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
            feed.data.map(value => {
                // Render feed, kustomisasi sesuai kebutuhan.
                var idStatus = value.id.split('_')[1];
                var created_Date = new Date(value.created_time);
                if (value.message && value.story) {
                    $('#my_facebook').append(
                    '<div class="feed">' +
                        '<h3>' + value.message + '</h3>' +
                        '<h4>' + value.story + '</h4>' +
                        '<p>' + created_Date.toLocaleDateString() + ' at ' + created_Date.toLocaleTimeString() + '</p>' +
                        '<button class="btn btn-xs btn-danger" type="button" onclick="deleteStatus('+idStatus+')">delete status</button>' +
                    '</div>'
                    );
                } else if (value.message) {
                    $('#my_facebook').append(
                    '<div class="feed">' +
                        '<h3>' + value.message + '</h3>' +
                        '<p>' + created_Date.toLocaleDateString() + ' at ' + created_Date.toLocaleTimeString() + '</p>' +
                        '<button class="btn btn-xs btn-danger" type="button" onclick="deleteStatus('+idStatus+')">delete status</button>' +
                    '</div>'
                    );
                } else if (value.story) {
                    $('#my_facebook').append(
                    '<div class="feed">' +
                        '<h4>' + value.story + '</h4>' +
                        '<p>' + created_Date.toLocaleDateString() + ' at ' + created_Date.toLocaleTimeString() + '</p>' +
                        '<button class="btn btn-xs btn-danger" type="button" onclick="deleteStatus('+idStatus+')">delete status</button>' +
                    '</div>'
                    );
                }
            });
        });
    });
    } else {
        // Tampilan ketika belum login
        $('#my_facebook').html(
            '<div class="welcome_page">' +
            '<h1 class="display-4 text-center">Welcome to My Facebook App</h1>' +
            '<button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login with Facebook</button>' +
            '</div>'
        );
    }
};

function facebookLogin(){
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
        console.log(response);
        render(true);
    }, {scope:'public_profile,user_posts,user_about_me,email,publish_actions'})
}

function facebookLogout(){
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
       if (response.status === 'connected') {
            FB.logout(function(response) {
                console.log(response);
                render(false);
            });
       }
    });
}

function getUserData(fun){
    // TODO: Lengkapi Method Ini
    // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
    // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
    // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
    // meneruskan response yang didapat ke fungsi callback tersebut
    // Apakah yang dimaksud dengan fungsi callback?
    FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
            FB.api('/me?fields=id,name,cover,picture.width(500).height(500),about,email,gender', 'GET', function(response){
                userID = response.id;
                fun(response);
                console.log(response.name + 'success login');
           });
        }
    });
}

const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.api('/me/feed', function(response) {
        if (response && !response.error) {
            fun(response);
        } else {
            console.log('error : ' + response);
        }
    });
};

function postFeed(message) {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {"message":message}, function(response) {
        if (response && !response.error) {
            console.log('Post success');
            render(true);
        }
    });
}


const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    $('#postInput').val('');
};

const deleteStatus = (idStatus) => {
    // Method untuk menghapus suatu status
    // mengambil id status dari tiap feed dan dijadikan
    // parameter untuk menjalankan method ini
    /* make the API call */
    FB.api("/"+userID+"_"+idStatus, "delete", function(response){
      if (response && !response.error) {
        console.log ('success delete status with id: '+idStatus);
        render(true);
      } else {
          console.log('gagal')
      }
    });
  };
